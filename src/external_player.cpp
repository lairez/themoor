
#include <chrono>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>

#include "external_player.hpp"
#include "board.hpp"            // for string_to_move

ExternalPlayer::ExternalPlayer(Color color, long time, const std::string &cmd)
  : color(color), time(std::chrono::milliseconds(time)) {
  if (color == BLACK) {
    colorstr = "black";
    othercolorstr = "white";
  } else {
    colorstr = "white";
    othercolorstr = "black";
  }

  auto pipe_out = std::make_unique<boost::process::ipstream>();
  auto pipe_in = std::make_unique<boost::process::opstream>();

  child_process =
    boost::process::child(cmd, boost::process::std_out > *pipe_out,
                          boost::process::std_in < *pipe_in);

  out = std::move(pipe_out);
  in = std::move(pipe_in);

  //reading_thread = std::thread(&ExternalPlayer::reading_worker, this);
}

ExternalPlayer::~ExternalPlayer() {
  // It is not enough to close the streams, we need to close the pipes.
  in->pipe().close();
  out->pipe().close();
//  reading_thread.join();
  child_process.terminate();
}

void ExternalPlayer::send_command(const std::string &cmd) {
  std::cout << colorstr << " << " << cmd << std::endl;
  *in << cmd << std::endl;
}

std::string ExternalPlayer::get_response() {

  // we wait for the answer, but no more that `time`
  auto start = std::chrono::steady_clock::now();
  // if (!semaphore.try_acquire_for(std::chrono::seconds(1))) {
  //   throw Timeout();
  // }

  read_response();

  auto end = std::chrono::steady_clock::now();

  time -= end - start;
  if (time < std::chrono::milliseconds(0)) {
    throw Timeout();
  }

  if (responses.empty())
    throw std::runtime_error("no response");

  auto response = std::move(responses.front());
  responses.pop_front();

  return response;
}

void ExternalPlayer::read_response() {

    std::string response;
    bool has_started = false;

    // append lines to responses until an empy line is met
    std::string line;
    while (true) {
      std::getline(*out, line);

      if (out->eof())
        return;

      if (line.empty()) {
        if (has_started)
          break;
        else
          continue;
      }

      has_started = true;

      // we do this check to remove some edax messages
      if (!line.empty() && line[0] != '*') {
        response += line;
        response.push_back('\n');
      }

      // logging
      std::cout << colorstr << " >> " << line << '\n';
    }

    responses.emplace_back(std::move(response));
    response.clear();
    std::cout.flush();
    // std::cout << "sem +" << std::endl;
}

int ExternalPlayer::genmove() {
  auto time_s = std::chrono::duration_cast<std::chrono::seconds>(time);
  //send_command("time_left " + colorstr + " " + std::to_string(time_s.count()));
  //get_response();

  send_command("genmove " + colorstr);
  auto response = get_response();

  auto response_stream = std::istringstream(response);
  char response_type = '?';
  response_stream >> response_type;

  if (response_type != '=')
    throw std::runtime_error("error from client: " + response);

  std::string move;
  response_stream >> move;

  if(!response_stream)
    throw std::runtime_error("error in reading move");

  return string_to_move(move);
}

void ExternalPlayer::otherplay(int i) {
  send_command("play " + othercolorstr + " " + move_to_string(i));
  get_response();
}
