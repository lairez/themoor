
#include <iostream>
#include <string>
#include <memory>

#include "color.hpp"
#include "engine.hpp"
#include "engine_random.hpp"
#include "engine_alphabeta.hpp"
#include "engine_mcts.hpp"


int main(int argc, char **argv) {
  std::string engine_name;
  if(argc >= 1)
    engine_name = argv[1];
  else
    engine_name = "random";


  std::unique_ptr<Engine> engine;
  if (engine_name == "random")
    engine = std::make_unique<EngineRandom>();
  else if (engine_name == "alphabeta")
    engine = std::make_unique<EngineAlphabeta>();
  else if (engine_name == "mcts")
    engine = std::make_unique<EngineMcts>();


  std::string command;
  while (std::cin >> command) {
    if (command == "time_left") {
      Color color;
      long amount;
      std::cin >> color >> amount;
      engine->timeleft(color, std::chrono::seconds(amount));
      std::cout << "=";
    } else if (command == "play") {
      Color color;
      std::string move;
      std::cin >> color >> move;
      engine->play(color, move);
      std::cout << "=";
    } else if (command == "genmove") {
      Color color;
      std::cin >> color;
      std::cout << "= " << move_to_string(engine->genmove(color)) << "\n";
    }
    std::cout << '\n' << std::endl;
  }

  return 0;
}
