#include "engine.hpp"

Engine::~Engine() {}

void Engine::play(Color c, std::string move) {
  if (c != board.current_player())
    throw std::invalid_argument("wrong color");
  int i = string_to_move(move);

  board.play(i);
  input_move(i);
};

int Engine::genmove(Color c) {
  if (c != board.current_player())
    throw std::invalid_argument("wrong color");

  int move = this->bestmove_timer(c);

  print_stats(std::cerr);
  play(c, move_to_string(move));

  return move;
}

int Engine::bestmove_timer(Color c) {
  auto start = std::chrono::steady_clock::now();
  int move = bestmove(c);
  auto delta = std::chrono::steady_clock::now() - start;

  if (c == WHITE)
    white_time -= delta;
  else
    black_time -= delta;

  total_time += delta;

  return move;
}
