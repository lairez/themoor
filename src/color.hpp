#ifndef COLOR_H_
#define COLOR_H_

#include <iostream>
#include <string>

enum Color { BLACK, WHITE };


std::ostream &operator<<(std::ostream &out, Color c);
std::istream &operator>>(std::istream &in, Color &c);

#endif // COLOR_H_
