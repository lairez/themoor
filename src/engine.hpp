#ifndef ENGINE_H_
#define ENGINE_H_

#include <chrono>
#include <iostream>

#include "board.hpp"
#include "color.hpp"

class Engine {

protected:
  std::chrono::steady_clock::duration white_time;
  std::chrono::steady_clock::duration black_time;
  std::chrono::steady_clock::duration total_time;

  Board board;

public:
  virtual ~Engine();

  template <typename T> void timeleft(Color c, T amount) {
    if (c == BLACK)
      black_time = amount;
    else if (c == WHITE)
      white_time = amount;
  }

  virtual void play(Color c, std::string move);
  virtual int genmove(Color c);
  virtual int bestmove(Color c) = 0;
  virtual void print_stats(std::ostream &out){};

  virtual void input_move(int move){};

  int bestmove_timer(Color c);
};

#endif // ENGINE_H_
