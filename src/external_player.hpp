#ifndef PLAYER_H_
#define PLAYER_H_

#include <ctime>
#include <deque>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

#include <list>

#include <boost/process.hpp>

class ExternalPlayer {

public:
  enum Color { BLACK = 0, WHITE = 1 };

  // instance members

  const Color color;

  std::chrono::steady_clock::duration time; // remaining time

  // instance methods

  ExternalPlayer(Color color, long time, const std::string &cmd);
  ~ExternalPlayer();

  int genmove(); // blocking operation
  void otherplay(int i);

private:
  std::unique_ptr<boost::process::ipstream>
      out; // This is the out stream of the player, so
           // an in stream for us,
  std::unique_ptr<boost::process::opstream> in; // and vice-versa.

  boost::process::child child_process; // subprocess running the engine

  // std::thread reading_thread; // thread in charge of reading the responses of
  //  the child process

  // semaphore to communicate with the reading thread
  // std::counting_semaphore<std::numeric_limits<int>::max()> semaphore{0};

  std::deque<std::string> responses;

  std::string colorstr, othercolorstr; // black and white or vice-versa

  void send_command(const std::string &cmd);
  std::string get_response();

  void read_response();

  struct Timeout : public std::exception {
    virtual const char *what() const throw() { return "Timeout"; }
  };
};

#endif // PLAYER_H_
