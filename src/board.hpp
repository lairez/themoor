#ifndef BOARD_H_
#define BOARD_H_

#include <bit> // for std::popcount and std::countr
#include <bitset>
#include <iostream>
#include <random>
#include <stdexcept> // for std::invalid_argument
#include <string>
#include <vector>
#include <array>

#include "color.hpp"

typedef std::bitset<64> bitboard;

///
/// A structure to represent the directions along which the disks are flipped.
///
struct Propagator {
  /// all propagators for othello game
  static std::vector<Propagator> all;

  const int leftshift;
  const int rightshift;
  const bitboard floodmask;

  bitboard shift(bitboard bits) {
    // bitset seems to do bound checks on leftshift and rightshift, so we
    // convert to ulong
    return bitboard((bits.to_ulong() << leftshift) >> rightshift) & floodmask;
  }
};

struct Board {
  static constexpr std::array<int, 4> corners = {0, 7, 56, 63};

  bitboard cur;
  bitboard other;
  bitboard legal;

  int8_t count = 0;
  int8_t diff = 0;

  bool blacktoplay;
  bool gameover = false;
  bool previousplayerskipped = false;

  Board();
  Board(bitboard cur, bitboard other, bool blacktoplay);

  void play(int i) noexcept;
  void skip() noexcept;

  void swap();

  bool is_legal_move(int move) const;
  int random_legal_move() const;
  void check_double_pass();

  int cur_score() const;
  int other_score() const;
  int black_score() const;
  int white_score() const;

  Color current_player() { return blacktoplay ? BLACK : WHITE; }

  bool operator==(const Board &b) const {
    return cur == b.cur && other == b.other;
  };

private:
  bitboard compute_legal_moves() const;
};

std::string move_to_string(int i);
int string_to_move(const std::string &move);

std::ostream &operator<<(std::ostream &out, const Board &board);

#endif // BOARD_H_
