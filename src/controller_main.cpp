#include <iostream>
#include <string>
#include <utility> // swap

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "board.hpp"
#include "external_player.hpp"

int main(int ac, char **av) {

  long blacktime = 100000, whitetime = 100000;
  std::string blackcmd, whitecmd;

  // Parsing options

  po::options_description desc("Allowed options");
  desc.add_options()("time,t", po::value<long>(),
                     "time for each player (in seconds) [default: 30]")(
      "black-time", po::value<long>(), "time for black player (in seconds)")(
      "white-time", po::value<long>(), "time for white player (in seconds)")(
      "black", po::value<std::string>(&blackcmd)->required(),
      "command for black player")("white",
                                  po::value<std::string>(&whitecmd)->required(),
                                  "command for white player");

  po::variables_map vm;
  po::store(po::parse_command_line(ac, av, desc), vm);
  po::notify(vm);

  if (vm.count("time")) {
    blacktime = whitetime = vm["time"].as<long>() * 1000;
  }

  if (vm.count("black-time")) {
    blacktime = vm["black-time"].as<long>() * 1000;
  }

  if (vm.count("white-time")) {
    whitetime = vm["white-time"].as<long>() * 1000;
  }

  // Let's go
  Board board;
  ExternalPlayer black(ExternalPlayer::BLACK, blacktime, blackcmd),
      white(ExternalPlayer::WHITE, whitetime, whitecmd);

  ExternalPlayer *cur = &black, *other = &white;

  while (!board.gameover) {
    int move = cur->genmove();
    board.play(move);
    other->otherplay(move);

    std::cout << board << std::endl;
    std::swap(cur, other);
    board.check_double_pass();
  }

  int bscore = board.black_score(), wscore = board.white_score();

  std::printf("FINAL SCORE: BLACK %d - %d WHITE\n", bscore, wscore);

  return 0;
}
