#include "engine_mcts.hpp"

#include <algorithm>
#include <cmath>

void Node::expand() {
  if (board.gameover || !children.empty())
    return;

  // std::cerr << "expanding..." << std::endl;
  if (board.is_legal_move(-1)) {
    Board next = board;
    next.play(-1);
    children.emplace_back(-1, next, this);
  } else {
    children.reserve(board.legal.count());
    for (int i = 0; i < 64; i++) {
      if (board.is_legal_move(i)) {
        Board next = board;
        next.play(i);
        children.emplace_back(i, next, this);
      }
    }
    std::random_shuffle(children.begin(), children.end());
  }
}

Edge *Node::select() {
  for (auto it = children.begin(); it != children.end(); it++) {
    if (it->node->runs == 0)
      return &*it;
  }

  auto best = children.end();
  float bestw = -std::numeric_limits<float>::infinity();
  float logruns = std::log2((float)runs);
  for (auto it = children.begin(); it != children.end(); it++) {
    float invn = 1.0f / it->node->runs;
    float w = (1.0f - float(it->node->wins) * invn) + std::sqrt(logruns * invn)*2;
    if (w > bestw) {
      bestw = w;
      best = it;
    }
  }

  return &*best;
}

Node *Node::select_leaf(bool expand) {
  Node *cur = this;
  while (!cur->children.empty()) {
    cur = cur->select()->node.get();
  }

  if (expand && !cur->board.gameover) {
    cur->expand();
    return cur->select()->node.get();
  } else {
    return cur;
  }
}

long Node::simulate(long nb) {
  if (board.gameover) {
    if (board.diff == 0)
      return nb / 2;
    else if (board.diff > 0)
      return nb;
    else
      return 0;
  }

  long newruns = 0, newwins = 0, ties = 0;
  Color player = board.current_player();

  while (nb--) {
    Board b = board;
    while (!b.gameover)
      b.play(b.random_legal_move());

    if (b.diff == 0)
      ties++;
    else if (b.diff < 0 && player != b.current_player())
      newwins++;
    else if (b.diff > 0 && player == b.current_player())
      newwins++;
  }

  return newwins + ties / 2;
}

void Node::backprop(long newruns, long newwins) {
  // if (newruns < newwins || newwins < 0)
  //   throw std::invalid_argument("please double check arguments to backprop");
  //std::cerr << "backprop... ("<< newwins << " / " << newruns << " )\n " << board << std::endl;
  runs += newruns;
  wins += newwins;
  if (parent)
    parent->backprop(newruns, newruns - newwins);
}

int EngineMcts::bestmove(Color c) {
  if (c != board.current_player())
    throw std::runtime_error("Unexpected, but rather catch it early");

  long N = 100000;
  const long nbruns = 10;
  while (N--) {
    // std::cerr << N << std::endl;
    // std::cerr << "selection... ";
    auto leaf = root->select_leaf(true);
    //std::cerr << "simulation... \n";
    //std::cerr << leaf->board << std::endl;
    long wins = leaf->simulate(nbruns);
    //std::cerr << wins << " wins / " << nbruns << " runs\n";
    // std::cerr << "propagation... ";
    leaf->backprop(nbruns, wins);
  }

  for (auto c : root->children) {
    std::cerr << "(" << move_to_string(c.move) << ":" << c.node->wins << "/"
              << c.node->runs << ") ";
  }
  std::cerr << std::endl;

  auto best = std::max_element(
      root->children.cbegin(), root->children.cend(),
      [](auto &c1, auto &c2) { return c1.node->runs < c2.node->runs; });

  return best->move;
}

void EngineMcts::input_move(int move) {
  root->expand();
  auto it = std::find_if(root->children.begin(), root->children.end(),
                         [=](auto &c) { return c.move == move; });
  if (it == root->children.end())
    throw std::runtime_error("illegal move or faulty expansion");

  root = it->node;
  root->parent = nullptr;
}
