#ifndef ENGINE_MCTS_H_
#define ENGINE_MCTS_H_

#include "engine.hpp"
#include <memory>

struct Edge;

struct Node {
  Board board;
  long runs = 0;
  long wins = 0;                // from the point of view of board.current_player()

  std::vector<Edge> children;
  Node *parent;

  Node(const Board &board, Node *parent) : board(board), parent(parent){};

  Node(const Node &n) = delete;
  Node(Node &&n) = delete;
  Node() = delete;

  void expand();
  Edge *select();
  Node *select_leaf(bool expand);

  long simulate(long nb);
  void backprop(long newruns, long newwins);
};

struct Edge {
  int move;
  std::shared_ptr<Node> node;

  Edge(int move, const Board &board, Node *parent)
      : move(move), node(std::make_shared<Node>(board, parent)) {}
};

class EngineMcts : public Engine {
  virtual int bestmove(Color c) override;
  virtual void input_move(int move) override;

protected:
  std::shared_ptr<Node> root = std::make_shared<Node>(Board(), nullptr);
};

#endif // ENGINE_MCTS_H_
