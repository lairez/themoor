#include <cstdint>
#include <cstdio>
#include <iostream>
#include <random>
#include <vector>

#include "board.hpp"

auto compute_propagators() {

  std::vector<Propagator> propagators;

  for (int sign : {-1, 1}) {
    for (int shift : {1, 7, 8, 9}) {
      bitboard mask = 0;
      for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
          int idx = 8 * i + j;
          int sidx = idx + shift * sign;
          int si = sidx / 8;
          int sj = sidx % 8;
          if (sidx >= 0 && sidx < 64 &&
              (si - i) * (si - i) + (sj - j) * (sj - j) <= 2) {
            mask.set(sidx, true);
          }
        }
      }
      propagators.push_back(Propagator{.leftshift = sign < 0 ? 0 : shift,
                                       .rightshift = sign < 0 ? shift : 0,
                                       .floodmask = mask});
    }
  }

  return propagators;
}

std::vector<Propagator> Propagator::all = compute_propagators();

Board::Board() : cur(0), other(0), blacktoplay(true) {
  other.set(3 * 8 + 3);
  other.set(4 * 8 + 4);
  cur.set(3 * 8 + 4);
  cur.set(4 * 8 + 3);

  count = 4;
  diff = 0;

  legal = compute_legal_moves();
}

Board::Board(bitboard cur, bitboard other, bool blacktoplay)
    : cur(cur), other(other), blacktoplay(blacktoplay) {
  legal = compute_legal_moves();
}

void Board::play(int i) noexcept {
  if (i < 0) [[unlikely]] {
    skip();
    return;
  }

  // if (!legal[i]) [[unlikely]]
  //   throw std::invalid_argument("illegal move");

  cur[i] = true;
  for (auto propagator : Propagator::all) {
    int8_t delta = 0;
    bitboard disk(0), flip(0);
    disk[i] = true;
    disk = propagator.shift(disk);
    while ((disk & other).any()) {
      delta++;
      flip |= disk;
      disk = propagator.shift(disk);
    }
    if ((disk & cur).any()) {
      cur |= flip;
      other &= flip.flip();
      diff += delta;
    }
  }

  count++;
  if (count == 64 || other.count() == 0)
    gameover = true;

  previousplayerskipped = false;
  swap();
}

void Board::skip() noexcept {
  // if (legal.any()) {
  //   throw std::invalid_argument("cannot skip");
  // }

  if (previousplayerskipped)
    gameover = true;

  previousplayerskipped = true;
  swap();
}

bool Board::is_legal_move(int move) const {
  if (legal.none())
    return move < 0;
  else
    return move >= 0 && move < 64 && legal[move];
}

int Board::random_legal_move() const {
  static std::random_device rd;
  static std::uniform_int_distribution<uint64_t> random_u64;
  static auto random_generator = std::minstd_rand(random_u64(rd));

  if (legal.none()) {
    return -1;
  }

  uint64_t selection = legal.to_ulong();

  while (1 < ((std::bitset<64>)selection).count()) {
    auto next = selection;
    next &= random_u64(random_generator);
    if (next)
      selection = next;
  }

  int idx = std::__countr_zero(selection);

  return idx;
}

void Board::check_double_pass() {
  if (legal.none()) {
    swap();
    if (legal.none()) {
      gameover = true;
    }
    swap();
  }
}

inline bitboard Board::compute_legal_moves() const {
  bitboard legal = 0, prop = 0;
  for (auto propagator : Propagator::all) {
    prop = propagator.shift(cur) & other;

    while (prop.any()) {
      prop = propagator.shift(prop);
      legal |= prop;
      prop &= other;
    }
  }
  legal &= ~cur;
  legal &= ~other;
  return legal;
}

void Board::swap() {
  std::swap(cur, other);

  blacktoplay = !blacktoplay;
  legal = compute_legal_moves();
  diff = -diff;
}

int Board::cur_score() const {
  if (other.count() == 0)
    return 64;
  else
    return cur.count();
}

int Board::other_score() const {
  if (cur.count() == 0)
    return 64;
  else
    return other.count();
}

int Board::black_score() const {
  if (blacktoplay) {
    return cur_score();
  } else {
    return other_score();
  }
}

int Board::white_score() const {
  if (blacktoplay) {
    return other_score();
  } else {
    return cur_score();
  }
}

void imprint(std::vector<std::string> &stringboard, bitboard bitboard,
             std::string str) {
  for (int i = 0; i < 8; i++)
    for (int j = 0; j < 8; j++) {
      int idx = 8 * i + j;
      if (bitboard[idx])
        stringboard[idx] = str;
    }
}

void print_stringboard(std::ostream &out, std::vector<std::string> stringboard,
                       const Board &board) {
  out << "    a b c d e f g h \n";
  out << "  ┏━━━━━━━━━━━━━━━━━┓\n";
  for (int i = 0; i < 8; i++) {
    out << i + 1 << " ┃ ";
    for (int j = 0; j < 8; j++) {
      out << stringboard[8 * i + j] << " ";
    }
    out << "┃";

    if (i == 3) {
      if (!board.gameover)
        out << "   " << (board.blacktoplay ? "black" : "white") << " to play";
      else
        out << "   gameover";
    } else if (i == 5) {
      out << "   black score: " << board.black_score();
    } else if (i == 6) {
      out << "   white score: " << board.white_score();
    }

    out << "\n";
  }
  out << "  ┗━━━━━━━━━━━━━━━━━┛\n";
}

std::ostream &operator<<(std::ostream &out, const Board &board) {
  bitboard white = board.cur;
  bitboard black = board.other;
  if (!board.blacktoplay)
    std::swap(white, black);

  auto stringboard = std::vector<std::string>(64, " ");

  imprint(stringboard, white, "○");
  imprint(stringboard, black, "●");

  imprint(stringboard, board.legal, "+");

  print_stringboard(out, stringboard, board);

  return out;
}

std::string move_to_string(int i) {
  if (i < 0)
    return "pass";

  if (i >= 64)
    throw std::invalid_argument("bad move");

  char column = 'a' + (i % 8);
  char row = '1' + i / 8;

  return std::string({column, row});
}

int string_to_move(const std::string &move) {
  if (move == "pass" || move == "skip")
    return -1;

  if (move.size() < 2)
    throw std::invalid_argument("bad move");

  char column = move[0], row = move[1];

  if (column < 'a' || column > 'h' || row < '1' || row > '8')
    throw std::invalid_argument("bad coordinates");

  return (column - 'a') + 8 * (row - '1');
}
