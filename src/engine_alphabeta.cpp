#include "engine_alphabeta.hpp"

#include <algorithm>
#include <limits>

int EngineAlphabeta::bestmove(Color c) {
  int move;
  eval = evalrec(8, board, move, std::numeric_limits<long>::min() + 1,
                 std::numeric_limits<long>::max());
  return move;
}

long EngineAlphabeta::evaluation(const Board &board) {
  long ply = board.count;

  // static std::random_device rd;
  // static std::uniform_int_distribution<long> random_long(-8,8);
  // static auto random_generator = std::minstd_rand(random_long(rd));

  long corner_eval = 0;
  for(auto c : Board::corners) {
    corner_eval = board.cur[c] - board.other[c];
  }

  return board.cur.count() + 100 * (64 - ply) * board.legal.count() + (64-ply)*1000*corner_eval;
}

long EngineAlphabeta::evalrec(int depth, Board &board, int &move, long alpha,
                              long beta) {
  nodestat++;
  if (depth == 0 || board.gameover) {
    return evaluation(board);
  }

  if (board.is_legal_move(-1)) {
    Board nextboard = board;
    nextboard.play(-1);
    move = -1;
    int ignoreme;
    return evalrec(depth - 1, nextboard, ignoreme, -beta, -alpha);
  }


  size_t nblegal = board.legal.count();
  int legal_moves[nblegal];
  auto legal_begin = &legal_moves[0];
  auto legal_end = legal_begin + nblegal;
  auto legal_cur = legal_begin;
  for (int i = 0; i < 64; i++) {
    if (board.is_legal_move(i))
      *legal_cur++ = i;
  }

  auto &heat_map = board.current_player() == BLACK ? heat_black : heat_white;
  double weight = 1L << 2*depth;

  std::sort(legal_begin, legal_end, [&](int i, int j){ return heat_map[i] > heat_map[j]; });

  int bestmove = -100;
  long besteval = std::numeric_limits<long>::min();

  for (auto it = legal_begin; it != legal_end; it++) {
    if(depth == 10)
      std::cerr << "Move " << move_to_string(*it) << " (heat: " << heat_map[*it] << ")" << std::endl;

    int ignoreme;
    Board nextboard = board;
    nextboard.play(*it);
    long eval = -evalrec(depth - 1, nextboard, ignoreme, -beta, -alpha);

    heat_map[*it] += eval;

    if (eval >= beta)
      return beta;

    alpha = std::max(alpha, eval);

    if (eval > besteval) {
      bestmove = *it;
      besteval = eval;
    }
  }

  move = bestmove;

  return besteval;
}

void EngineAlphabeta::print_stats(std::ostream &out) {
  out << "Number of explored nodes: " << nodestat << std::endl;
  out << "Total thinking time: "
      << std::chrono::duration<float>(total_time).count() << 's' << std::endl;
  out << "Current evaluation: " << eval << " (" << evaluation(board) << ")" << std::endl;
}
