#ifndef RANDOM_H_
#define RANDOM_H_

#include "engine.hpp"

class EngineRandom : public Engine {
public:
  virtual int bestmove(Color c) override { return board.random_legal_move(); }
};

#endif // RANDOM_H_
