#ifndef ENGINE_ALPHABETA_H_
#define ENGINE_ALPHABETA_H_

#include "engine.hpp"

#include <vector>

class EngineAlphabeta : public Engine {

public:
  virtual long evaluation(const Board &board);
  virtual int bestmove(Color c) override;
  virtual void print_stats(std::ostream &out) override;

protected:
  long evalrec(int depth, Board &board, int &move, long alpha, long beta);

  long nodestat = 0;
  long eval = 0;

  std::vector<double> heat_black = std::vector<double>(64);
  std::vector<double> heat_white = std::vector<double>(64);
};

#endif // ENGINE_ALPHABETA_H_
