#include "color.hpp"

std::ostream &operator<<(std::ostream &out, Color c) {
  out << (c == BLACK ? "black" : "white");
  return out;
}

std::istream &operator>>(std::istream &in, Color &c) {
  std::string input;
  in >> input;
  if (input == "black")
    c = BLACK;
  else if (input == "white")
    c = WHITE;
  else
    in.setstate(std::ios::failbit);
  return in;
}
